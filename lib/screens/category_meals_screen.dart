import 'package:flutter/material.dart';

import '../models/meal.dart';
import '../widgets/meal_item.dart';

class CategoryMealsScreen extends StatefulWidget {
  static const routeName = '/category-meals';

  final List<Meal> availableMeals;

  CategoryMealsScreen(this.availableMeals);

  @override
  _CategoryMealsScreenState createState() => _CategoryMealsScreenState();
}

class _CategoryMealsScreenState extends State<CategoryMealsScreen> {
  String categoryTitle;
  List<Meal> categorizedMeals;
  bool _loadedInitData = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (!_loadedInitData) {
      final routeArgs =
          ModalRoute.of(context).settings.arguments as Map<String, String>;
      final categoryId = routeArgs['id'];

      categoryTitle = routeArgs['title'];
      categorizedMeals = widget.availableMeals
          .where((meal) => meal.categories.contains(categoryId))
          .toList();

      _loadedInitData = true;
    }
  }

  void _removeMeal(String mealId) {
    setState(() {
      categorizedMeals.removeWhere((meal) => meal.id == mealId);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(categoryTitle),
      ),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          //returns a single row/list item
          return MealItem(
            id: categorizedMeals[index].id,
            title: categorizedMeals[index].title,
            imgUrl: categorizedMeals[index].imageUrl,
            prepTime: categorizedMeals[index].prepTime,
            complexity: categorizedMeals[index].complexity,
            affordability: categorizedMeals[index].affordability,
          );
        },
        itemCount: categorizedMeals.length,
      ),
    );
  }
}
