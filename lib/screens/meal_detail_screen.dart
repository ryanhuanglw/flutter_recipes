import 'package:flutter/material.dart';

import '../dummy_data.dart';

class MealDetailScreen extends StatelessWidget {
  static const routeName = '/meal-details';

  final Function toggleFavorite;
  final Function isFavorite;

  MealDetailScreen(this.toggleFavorite, this.isFavorite);

  Widget buildSectionTitle(String text, BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Text(
        text,
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }

  Widget buildContainer(Widget child, screenSize) {
    return ConstrainedBox(
      constraints: BoxConstraints(
        maxHeight: screenSize.height * 0.35,
      ),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(10)),
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.all(10),
        width: screenSize.width,
        child: child,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final routeArguments =
        ModalRoute.of(context).settings.arguments as Map<String, String>;
    final id = routeArguments['id'];
    final selectedMeal = DUMMY_MEALS.firstWhere((meal) => meal.id == id);

    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text('${selectedMeal.title}'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: screenSize.height * 0.3,
              width: screenSize.width,
              child: Image.network(
                selectedMeal.imageUrl,
                fit: BoxFit.fitWidth,
              ),
            ),
            buildSectionTitle('Ingredients', context),
            buildContainer(
              ListView.builder(
                shrinkWrap: true,
                itemBuilder: (ctx, index) => Card(
                  color: Theme.of(context).accentColor,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 15,
                      vertical: 5,
                    ),
                    child: Text(
                      '${selectedMeal.ingredients[index]}',
                    ),
                  ),
                ),
                itemCount: selectedMeal.ingredients.length,
              ),
              screenSize,
            ),
            buildSectionTitle('Instructions', context),
            buildContainer(
              ListView.builder(
                shrinkWrap: true,
                itemBuilder: (ctx, index) => Column(
                  children: <Widget>[
                    ListTile(
                      leading: CircleAvatar(
                        child: Text('#${index + 1}'),
                      ),
                      title: Text(
                        selectedMeal.instructions[index],
                      ),
                    ),
                    Divider(
                      color: Colors.grey[700],
                    )
                  ],
                ),
                itemCount: selectedMeal.instructions.length,
              ),
              screenSize,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          isFavorite(id) ? Icons.star : Icons.star_border,
        ),
        onPressed: ()=>toggleFavorite(id),
      ),
    );
  }
}
