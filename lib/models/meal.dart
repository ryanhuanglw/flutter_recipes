import 'package:flutter/foundation.dart';

enum Complexity {
  Simple,
  Medium,
  Hard,
  Expert,
}

enum Affordability {
  Affordable,
  Average,
  Pricey,
  ExpensiveAF,
}

class Meal {
  final String id;
  final List<String> categories;
  final String title;
  final String imageUrl;
  final List<String> ingredients;
  final List<String> instructions;
  final int prepTime;
  final Complexity complexity;
  final Affordability affordability;

  final bool isGlutenFree;
  final bool isLactoseFree;
  final bool isVegan;
  final bool isHalal;

  const Meal({
    @required this.id,
    @required this.categories,
    @required this.title,
    @required this.imageUrl,
    @required this.ingredients,
    @required this.instructions,
    @required this.prepTime,
    @required this.complexity,
    @required this.affordability,
    @required this.isGlutenFree,
    @required this.isLactoseFree,
    @required this.isVegan,
    @required this.isHalal,
  });
}
